This repository was forked from https://github.com/msieg/deep-music-visualizer for educational purposes as part of the Digital and Soft Skills for Inerdisciplinary Projects Development (DSSIPD) course in the RWTH university, Aachen.

# Our Documentation

In order to run this repo, you will need:
1. _At the minimum_: A Windows 10/11 Laptop/PC with Anaconda3. A step-by-step guide can be found on the RWTH Moodle [here](https://moodle.rwth-aachen.de/mod/page/view.php?id=792212).
1. _Preferably_: A Windows 10/11 Laptop with a CUDA-accelerated graphics card. A step-by-step guide into installing CUDA can be found on the RWTH Moodle [here](https://moodle.rwth-aachen.de/mod/page/view.php?id=792213).

## I followed these steps, then what?

### Create a `repos` folder and clone the repo there

If you don't have a `repos` folder yet in your user's folder on Windows, please create one as follows:  
1. Navigate to `%USERPROFILE%` in your Windows Explorer, then right click and create a new folder. Name it `repos` and link it (by dragging and dropping) in the Quick Access side pane:  
![](./img/create_repos_folder.jpg)
1. Right click on the `repos` folder in the Quick Access side pane and choose _Git Bash Here_ as shown below:  
![](./img/git_bash_here.jpg)
1. In the resulting Git Bash shell, type in the following command:  
    `git clone <HTTPS clone URL>`
1. Navigate to the `deep-music-visualizer` folder in your user directory, making sure that it exists and that the files exist on your local filesystem.

## Run `visualize.py` from the Anaconda virtual environment

1. Run _Anaconda3_ from the start menu on your Windows 10/11 Laptop/PC
1. Switch to the environments tab
1. Right click on the environment that you setup in the Moodle guides, and choose _Open Terminal_ (this can be the one ending with `-gpu` in case you were using a machine with a CUDA-accelerated graphics card):
![](./img/environment_terminal.jpg)
1. Now we are ready to construct the command needed to run the visualizer. The command has the following parts:   
    `python {script filename} {script options}`
1. The `{script filename}` part above can be filled by dragging and dropping the file `visualize.py` from the Explorer to the shell terminal window:
![](./img/drag_drop_script.jpg) 
1. The `{script_options}` are thoroughly explained in the original documentation (see below). For our first test, we just need two.
1. The first option we need is the `--song` option, followed by a space, then the input WAV file (the music file). In this case, we could drag and drop the file `sample.wav` from the repo as shown below:
![](./img/drag_drop_wav.jpg)
1. In case of using CUDA acceleration, you will need to use another option, `--batch_size`, with a value >= 1. A higher number will make the visualizer work faster, but will need more memory.
1. On the _CR Master_ laptops (with a Quadro T1000 and 2GB of memory), the right (i.e. highest) value for the `--batch_size` is 4 without errors.

# Original Documentation (adapted from the original repository)

The Deep Music Visualizer uses BigGAN (Brock et al., 2018), a generative neural network, to visualize music. Like this:

[![Alt text](https://img.youtube.com/vi/L7R-yBZ5QYc/0.jpg)](https://www.youtube.com/watch?v=L7R-yBZ5QYc)

More examples: https://www.instagram.com/deep_music_visualizer/

## Installation

This repo has been tested on Python3

Assuming you have python installed, open terminal and run these commands:

```bash
git clone https://github.com/msieg/deep-music-visualizer.git
cd deep-music-visualizer
conda create -n deep-music-visualizer python=3.10
conda activate deep-music-visualizer
conda install -c conda-forge llvmlite libsndfile
pip install -r requirements.txt
```

If you are on linux, you may also need to run:

```bash
apt-get update
apt-get install ffmpeg
apt-get install libsndfile1
```

## Run in Colab
1. open https://colab.research.google.com/ in browser
1. upload deep_music_visualizer.ipynb and open 
1. upload your song file from left side menu _Files_.
1. change/add parameters in cell `args = parser.parse_args(`
1. change runtime type from menu `Runtime -> change runtiem type -> GPU` and connect
1. run cell by cell or from menu `Runtime -> Run all`
1. click outpu.mp4 to download

## How to run

All features of the visualizer are available as input parameters. Each parameter is described below.

### song

Audio file of type mp3, wav, or ogg.

This is the only required argument!

Example:

```bash
python visualize.py --song beethoven.mp3
```

### resolution

128, 256, or 512

Default: 512

If you are running on a CPU (if you're not sure, you are on a CPU), you might want to use a lower resolution or else the code will take a very long time to run. At 512X512, it will take ~7 HOURS to generate 1 minute of video on a standard desktop computer (assuming all other parameters are default). At 128x128, it would take ~25 minutes. To speed up runtime, you can decrease the resolution or increase the [frame_length](#Frame_length). To dramatically speed up runtime and generate higher quality videos, use a resolution of 512 on a [GPU on a google cloud virtual machine](https://cloud.google.com/deep-learning-vm/docs/cloud-marketplace).

Example:

```bash
python visualize.py --song beethoven.mp3 --resolution 128
```

### duration

Duration of the video output in seconds. It can be useful to generate shorter videos while you are tweaking the other visualizer parameters. Once you find your preferred parameters, remove the duration argument and set [use_previous_vectors](#use_previous_vectors) to 1 to generate the same video but for the full duration of the song. 

Default: Full length of the audio

Example:

```bash
python visualize.py --song beethoven.mp3 --duration 30
```

### pitch sensitivity

The pitch sensitivity controls how rapidly the class vector (thematic content of the video) will react to changes in pitch. The higher the number, the higher the sensitivity. 

Range: 1 – 299

Recommended range: 200 – 295

Default: 220

Example:

```bash
python visualize.py --song beethoven.mp3 --pitch_sensitivity 280
```

### tempo sensitivity

The tempo sensitivity controls how rapidly the noise vector (i.e. the overall size, position, and orientation of objects in the images) will react to changes in volume and tempo. The higher the number, the higher the sensitivity. 

Recommended range: 0.05 – 0.8

Default: 0.25

Example:

```bash
python visualize.py --song beethoven.mp3 --tempo_sensitivity 0.1
```

### depth

The depth specifies the max value of the class vector units. Numbers closer to 1 seem to yield more thematically rich content. Numbers closer to 0 seem to yield more 'deep' structures like human and dog faces. However, this depends heavily on the specific classes you are using.  

Range: 0.01 - 1

Default: 1

Example:

```bash
python visualize.py --song beethoven.mp3 --depth 0.5
```

### classes

If you want to choose which classes (image categories) to visualize, you can specify a list of ImageNet indices (1-1000) here. ([list of ImageNet class indices](https://gist.github.com/yrevar/942d3a0ac09ec9e5eb3a)). The number of classes must be equal to [num_classes] (default is twelve, corresponding to the twelve musical pitches (A, A#, B, etc.)). You can also enter the class indices in order of priority (highest priority first) and set [sort_classes_by_power](#sort_classes_by_power) to 1. 

Default: Twelve random indices between 0-999

Example (if num_classes is set to default of twelve):
```bash
python visualize.py --song beethoven.mp3 --classes 45 99 567 234 89 90 105 998 56 677 884 530
```

### num_classes

If you want to focus the visualizer around fewer than twelve themes, you can set num_classes to a number less than twelve. Since each class is associated with a pitch, the pitches that are retained when num_classes < 12 are those with the most overall power in the song. 

Default: 12

Example:
```bash
python visualize.py --song beethoven.mp3 --num_classes 4 
```

Or if you want to choose the classes:

```bash
python visualize.py --song beethoven.mp3 --num_classes 4 --classes 987 23 56 782
```

### sort_classes_by_power

Set this to 1 if you want to prioritize the classes based on the order that you entered them in the [class input](#classes). If you do not specify the class input, there is no reason to set this to 1. If you do specify the class input and do not set this to 1, the classes will be associated with the pitches in harmonic order from A, A#, B, etc.

Example:

```bash
python visualize.py --song beethoven.mp3  --classes 45 99 567 234 89 90 105 998 56 677 884 530 --sort_classes_by_power 1
```

### jitter

The jitter prevents the same exact noise vectors from cycling repetitively during repetitive music so that the video output is more interesting. If you do want to cycle repetitively, set jitter to 0.

Range: 0 – 1
Default: 0.5 

Example:

```bash
python visualize.py --song beethoven.mp3 --jitter 0
```

### frame_length

The frame length controls the number of audio frames per video frame in the output. If you want a higher frame rate for visualizing very rapid music, lower the frame_length. If you want a lower frame rate (perhaps if you are running on a CPU and want to cut down your runtime), raise the frame_length. The default of 512 is high quality. 

Range: Multiples of 2^6

Default: 512

Example:

```bash
python visualize.py --song beethoven.mp3 --frame_length 2048
```

### truncation

The truncation controls the variability of images that BigGAN generates by limiting the max values in the noise vector. Truncations closer to 1 yield more variable images, and truncations closer to 0 yield simpler images with more recognizable, normal-looking objects. 

Range: 0.1 - 1

Default: 1

Example:

```bash
python visualize.py --song beethoven.mp3 --truncation 0.4
```

### smooth_factor

After the class vectors have been generated, they are smoothed by interpolating linearly between the means of class vectors in bins of size [smooth_factor]. This is performed because small local fluctuations in pitch can cause the video frames to fluctuate back and forth. If you want to visualize very fast music with rapid changes in pitch, you can lower the smooth factor. You may also want to lower the frame_length in that case. However, for most songs, it is difficult to avoid rapid fluctuations with smooth factors less than 10. 

Range: > 0

Recommended range: 10 – 30

Default: 20

Example:

```bash
python visualize.py --song beethoven.mp3 --smooth_factor 6
```

### batch_size

BigGAN generates the images in batches of size [batch_size]. The only reason to reduce batch size from the default of 30 is if you run out of CUDA memory on a GPU. Reducing the batch size will slightly increase overall runtime. 

Default: 30

Example:

```bash
python visualize.py --song beethoven.mp3 --batch_size 20
```

### use_previous_classes

If your previous run of the visualizer used random classes (i.e. you did not manually set the [class](#classes) input), and you liked the video output but want to mess with some other parameters, set use_previous_classes to 1 so that you create a similar video with the same classes on the next run of the code. 

Default: 0

Example:

```bash
python visualize.py --song beethoven.mp3 --use_previous_classes 1
```

### use_previous_vectors

If you're messing around with the visualizer parameters, it can be useful to generate videos with short durations to keep your runtime low. Once you find the right set of parameters, remove the [duration](#duration) argument and set use_previous_vectors to 1 to generate the same video again, but for a longer duration. 

Default: 0

Example:

```bash
python visualize.py --song beethoven.mp3 --use_previous_vectors 1
```

### output_file

Output file name

Default: output.mp4

Example:

```bash
python visualize.py --song beethoven.mp3 --output_file my_movie.mp4
```

